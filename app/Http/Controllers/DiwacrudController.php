<?php

namespace App\Http\Controllers;

use App\Models\diwacrud;
use Illuminate\Http\Request;
use Validator;
use DB;

class DiwacrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req){

            $val = Validator::make($req->all(),[
                'title' => 'required|max:255|unique:diwacruds',
                'description' => 'required|max:255',
            ]);
            if ($val->fails()) {
            return response()->json([
                'errors'    =>  $val->errors()
            ],400);
            }else{
                DB::beginTransaction();
                try{
                    diwacrud::create([
                        'title'         => $req['title'],
                        'description'   => $req['description']
                    ]);
                    DB::commit();
                    return response()->json([            
                        'message' => "Book Successfully Added!",
                        'data'=> $req['title']
                    ]);
                }catch(\Exception $ex){
                    DB::rollback();
                    return response()->json([
                        'errors'    =>  [ 'error diwaadd' ],
                        'message'   =>  $ex->getMessage()
                    ],500);
                }
            }


            
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\diwacrud  $diwacrud
     * @return \Illuminate\Http\Response
     */
    public function show(diwacrud $diwacrud)
    {
        //
    }

    public function booklist()
    {
        $booklist = diwacrud::get();
        return response()->json([
            'payload'    =>  $booklist
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\diwacrud  $diwacrud
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {

        


        $val = Validator::make($req->all(),[
            'title' => 'required|max:255',
            'description' => 'required|max:255',
        ]);
        if ($val->fails()) {
        return response()->json([
            'errors'    =>  $val->errors()
        ],400);
        }else{
            diwacrud::where('id', $req['id'])
            ->update([
                'title' => $req['title'],
                'description' => $req['description']
             ]);
            return response()->json([
            'message'    =>  "Data Successfully Edited!"
            ]);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\diwacrud  $diwacrud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, diwacrud $diwacrud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\diwacrud  $diwacrud
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        diwacrud::destroy($request['id']);
        return response()->json([
            'payload'    =>  $request['id']
        ]);
    }
}
